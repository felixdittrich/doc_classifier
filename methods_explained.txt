text_classifier: 

Latent Dirichlet Allocation (LDA): 
https://www.jmlr.org/papers/volume3/blei03a/blei03a.pdf     
https://en.wikipedia.org/wiki/Latent_Dirichlet_allocation

Text Summarization:
https://arxiv.org/abs/1910.13461
https://www.facebook.com/FacebookAI/videos/bart-model-summary-generation/969182586851981/

Word Embedding: 
https://www.tensorflow.org/tutorials/text/word_embeddings

Bert Word Embedding:
https://mccormickml.com/2019/05/14/BERT-word-embeddings-tutorial/

Transformer: 
https://www.youtube.com/watch?v=S27pHKBEp30   
http://jalammar.github.io/illustrated-transformer/

BERT / DistilBERT: 
https://arxiv.org/abs/1810.04805
https://arxiv.org/abs/1910.01108
https://www.kdnuggets.com/2019/09/bert-roberta-distilbert-xlnet-one-use.html
